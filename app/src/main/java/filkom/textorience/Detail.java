package filkom.textorience;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.filament.View;
import com.google.firebase.auth.FirebaseAuth;

public class Detail extends AppCompatActivity {

    TextView tvContent;
    TextView tvTitleDetail;
    TextView tvDateDetail;
    TextView tvEmailDetail;
    Button btnEditPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        btnEditPost = findViewById(R.id.btnEditPost);
        tvTitleDetail = findViewById(R.id.tvTitleDetail);
        tvContent = findViewById(R.id.tvContent);
        tvDateDetail = findViewById(R.id.tvDateDetail);
        tvEmailDetail = findViewById(R.id.tvEmailDetail);

        tvTitleDetail.setText(getIntent().getStringExtra("title"));
        tvContent.setText(getIntent().getStringExtra("content"));
        tvDateDetail.setText(getIntent().getStringExtra("date"));
        tvEmailDetail.setText(getIntent().getStringExtra("email"));

//        if (FirebaseAuth.getInstance().getCurrentUser().getEmail() == getIntent().getStringExtra("email")) {
//            btnEditPost.setVisibility(View.VISIBLE);
//            btnEditPost.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//        }

        
    }
}