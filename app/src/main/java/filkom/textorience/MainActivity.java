package filkom.textorience;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user == null) {
            startActivity(new Intent(this, Login.class));
        } else {
            //Initialize Bottom Navigation View.
            BottomNavigationView navView = findViewById(R.id.bottom_navigatin_view);

            //Pass the ID's of Different destinations
            AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.homeFragment, R.id.uploadFragment, R.id.profileFragment)
                    .build();

            //Initialize NavController.
            NavController navController = Navigation.findNavController(this, R.id.nav_fragment);
            NavigationUI.setupWithNavController(navView, navController);
        }
    }
}