package filkom.textorience;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class CreateAccount extends AppCompatActivity {

    Button btnSignup;
    TextView tvSignupError;
    EditText etEmail, etPassword, etConfirmPass;
    FirebaseAuth fireBaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        etEmail = findViewById(R.id.etEmailRegister);
        etPassword = findViewById(R.id.etPasswordRegister);
        etConfirmPass = findViewById(R.id.etConfirmPass);
        btnSignup = findViewById(R.id.btnRegister);
        tvSignupError = findViewById(R.id.tvRegisterError);
        fireBaseAuth = FirebaseAuth.getInstance();

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                String confirmPass = etConfirmPass.getText().toString();
                if (email.isEmpty()){
                    // email.setError("Email is empty");
                    // email.requestFocus();
                    tvSignupError.setText("Email is empty");
                    // return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    //email.setError("Enter the valid email address");
                    //email.requestFocus();
                    tvSignupError.setText("Email does not found");
                    // return;
                }
                if (password.isEmpty()) {
                    // password.setError("Enter the password");
                    // password.requestFocus();
                    tvSignupError.setText("Enter the password");
                    // return;
                }
                if (confirmPass.isEmpty()) {
                    // confirmPass.setError("Enter the confirm password");
                    // confirmPass.requestFocus();
                    tvSignupError.setText("Enter the confirm password");
                    // return;
                }
                if (!confirmPass.equals(password)) {
                    // confirmPass.setError("password is not same");
                    // confirmPass.requestFocus();
                    tvSignupError.setText("Password does not match");
                    // return;
                } else {
                    create(email, password);
                }
            }
        });
    }

    private void create(String email, String password) {
        fireBaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    // showToast("Authentication failed. " + task.getException());
                    tvSignupError.setText("Authentication failed");
                } else {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }
            }
        });
    }
}
