package filkom.textorience.rest;

import java.util.List;

import filkom.textorience.model.Like;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface LikesService {
    @GET("likes")
    Call<List<Like>> listLike();

    @GET("likes")
    Call<List<Like>> getLike(@Query("post_id") String post_id);

    @FormUrlEncoded
    @POST("likes")
    Call<Like> addLike(@Field("post_id")String post_id,
                       @Field("email")String email);
}
