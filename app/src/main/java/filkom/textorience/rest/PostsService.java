package filkom.textorience.rest;

import java.util.List;

import filkom.textorience.model.Post;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PostsService {
        @GET("posts")
        Call<List<Post>> listPost();

        @GET("posts")
        Call<List<Post>> getPost(@Query("email") String email);

        @FormUrlEncoded
        @POST("posts")
        Call<Post> addPost(@Field("title") String title,
                           @Field("email")String email,
                           @Field("content")String content,
                           @Field("posted_at")String posted_at);

        @FormUrlEncoded
        @PUT("posts")
        Call<Post> putPost(@Field("title") String title,
                             @Field("email")String email,
                             @Field("content")String content,
                             @Field("posted_at")String posted_at);
}
