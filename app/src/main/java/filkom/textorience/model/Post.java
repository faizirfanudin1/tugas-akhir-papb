package filkom.textorience.model;

public class Post {

    private String id, email, title, content, posted_at;

    public Post(String id, String email, String title, String content, String posted_at) {
        this.id = id;
        this.email = email;
        this.title = title;
        this.content = content;
        this.posted_at = posted_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPosted_at() {
        return posted_at;
    }

    public void setPosted_at(String posted_at) {
        this.posted_at = posted_at;
    }
}
