package filkom.textorience.model;

public class Like {

    private String id, post_id, email;

    public Like(String id, String post_id, String email) {
        this.id = id;
        this.post_id = post_id;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
