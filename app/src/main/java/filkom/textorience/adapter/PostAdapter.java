package filkom.textorience.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import filkom.textorience.Detail;
import filkom.textorience.Home;
import filkom.textorience.MainActivity;
import filkom.textorience.R;
import filkom.textorience.model.Like;
import filkom.textorience.model.Post;
import filkom.textorience.rest.LikesService;
import filkom.textorience.rest.PostsService;
import filkom.textorience.rest.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    LayoutInflater inflater;
    ArrayList<Post> items;
    Context context;

    public PostAdapter(Context context, ArrayList<Post> items) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.items = items;
    }

    @NonNull
    @Override
    public PostAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.layout_posts, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PostAdapter.ViewHolder holder, int position) {
        Post item = items.get(position);
        holder.tvEmail.setText(item.getEmail());
        holder.tvDate.setText(item.getPosted_at());
        holder.tvTitle.setText(item.getTitle());
        ArrayList<Like> likeList = new ArrayList<>();
        LikesService likesService = RetrofitClient.getClient().create(LikesService.class);
        Call<List<Like>> getLike = likesService.getLike(item.getId());
        getLike.enqueue(new Callback<List<Like>>() {
            @Override
            public void onResponse(Call<List<Like>> call, Response<List<Like>> response) {
                if (response.isSuccessful()) {
                    List<Like> list = response.body();
                    Log.d("success", "list" + list.size());
                    likeList.addAll(list);
                    holder.tvLikesAmmount.setText(String.valueOf(likeList.size()));
                } else {
                    Log.d("errt", "" + response.errorBody());
                    Toast.makeText(context, "" + response.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Like>> call, Throwable t) {
                Log.d("DataModel", "" + t.getMessage());
                Toast.makeText(context, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Detail.class);
                i.putExtra("email", item.getEmail());
                i.putExtra("date", item.getPosted_at());
                i.putExtra("title", item.getTitle());
                i.putExtra("content", item.getContent());
                context.startActivity(i);
            }
        });
        holder.btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LikesService likesService = RetrofitClient.getClient().create(LikesService.class);
                String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                retrofit2.Call<Like> addLike = likesService.addLike(item.getId(), email);
                addLike.enqueue(new Callback<Like>() {
                    @Override
                    public void onResponse(Call<Like> call, Response<Like> response) {
                        context.startActivity(new Intent(context, MainActivity.class));
                    }

                    @Override
                    public void onFailure(Call<Like> call, Throwable t) {
                        Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvEmail, tvDate, tvLikesAmmount;
        Button btnDetail;
        ImageButton btnLike;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvEmail = itemView.findViewById(R.id.tvLayoutEmail);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvLikesAmmount = itemView.findViewById(R.id.tvLikesAmount);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            btnDetail = itemView.findViewById(R.id.btnDetail);
            btnLike = itemView.findViewById(R.id.btnLike);
        }
    }
}
